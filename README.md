**Player hierarchical state machine project**

Hierarchical state machine implementation on example of music player. Uses POSIX IPC queue.

---

## Prerequisites

You will need following tools:

1. GCC
2. GNU Make
3. Shell
4. C POSIX library

---

## Building

Project uses standard make build system.

Building application:

*  Simply run **make** in root repository directory

Building test tool:

*  Run: **make SRC_DIRS=testTool TARGET_EXEC=playerTester** in root repository directory

Building all at once:

*  Execute **./makeAll** script in **scripts** directory

---

## Testing

Testing is done using separate **playerTester** tool, that opens the same IPC queue the **player** application uses. Tool sends event messages. Testing example:

1. Run **player** application: **./build/player/player**
2. Send **power_on** event message using **playerTester** tool: **./build/playerTester/playerTester on** and analyze output of **player**
2. Send **start_playback** event message: **./build/playerTester/playerTester play**

Running tests simpler way:

1. Run **./test** script in **scripts** directory
