#include <mqueue.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MSG_PRIORITY (1)
#define MSG_SIZE (64)

int main(int argc, char *argv[])
{
    char msg[MSG_SIZE];
    unsigned char argumentCorrect = 1;

    if (argc == 2)
    {
        if(strncmp(argv[1], "off", sizeof("off")) == 0)
        {
            msg [0] = 0;
        }
        else if(strncmp(argv[1], "on", sizeof("on")) == 0)
        {
            msg [0] = 1;
        }
        else if(strncmp(argv[1], "play", sizeof("play")) == 0)
        {
            msg [0] = 2;
        }
        else if(strncmp(argv[1], "stop", sizeof("stop")) == 0)
        {
            msg [0] = 3;
        }
        else
        {
            argumentCorrect = 0;
        }
    }
    else
    {
        argumentCorrect = 0;
    }

    if(argumentCorrect)
    {
        mqd_t mqDescriptor;
        unsigned int priority = MSG_PRIORITY; 

        mqDescriptor = mq_open("/player", O_RDWR);
        
        if(errno != 0)
        {
            perror("mq_open()");
        }

        if(mq_send(mqDescriptor, msg, MSG_SIZE, priority) == -1)
        {
            perror("mq_send()");
        }

        if(errno != 0)
        {
            perror("mq_send()");
        
        }

        mq_close(mqDescriptor);
    }
    else
    {
        printf("usage: playerTester <command>\n"
               "\n"
               "Queue has to be created before using. Available commands:\n"
               "\toff        Send power_off event\n"
               "\ton         Send power_on event\n"
               "\tplay       Send start_playback event\n"
               "\tstop       Send stop_playback event\n");
    }
    
    return 0;
}
