#include "State.h"

State::State(std::string _name) : parent(nullptr), name(_name)
{

}

State::State(State *parent, std::string _name) : name(_name)
{
    this->parent = parent;
}

bool State::onEvent(Event * event)
{
    return false;
}
