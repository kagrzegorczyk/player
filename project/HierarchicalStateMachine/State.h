#pragma once

/**
 * @file
 * @author  Karol Grzegorczyk
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * @section DESCRIPTION
 *
 * The State class represents an abstract state machine state.
 */

#include <string>

/// Abstract class used for events
struct Event {};

class State
{
public:
    /**
    * Creates a parentless state.
    *
    * @param _name      state name
    */
    State(std::string _name);

    /**
    * Creates a state.
    *
    * @param *parent    parent state pointer
    * @param _name      state name
    */
    State(State *parent, std::string name);

    /**
    * Event handler to be overriden
    *
    * @param *event     event pointer to be processed
    * @return           true if event was processed
    */
    virtual bool onEvent(Event * event);

    /// parent pointer
    State *parent;
    std::string name;
};
