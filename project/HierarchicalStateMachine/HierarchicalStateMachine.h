#pragma once

/**
 * @file
 * @author  Karol Grzegorczyk
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * @section DESCRIPTION
 *
 * The HierarchicalStateMachine class represents an abstract state machine.
 */

#include "State.h"

class HierarchicalStateMachine
{
public:
    /**
    * Creates state machine instance
    *
    * @param initialState      initial State pointer
    */
    HierarchicalStateMachine(State * initialState);

protected:
    /// Current State pointer
    State * currentState;

    /**
    * Sets a state.
    *
    * @param state      State pointer
    */
    void setState(State * state);

    /**
    * Processes the event passing it to current state and its ancestors if not consumed.
    *
    * @param *event     event pointer to be processed
    * @return           State pointer, that consument the event,returns nullptr if not processed
    */
    State * process(Event *event);
};
