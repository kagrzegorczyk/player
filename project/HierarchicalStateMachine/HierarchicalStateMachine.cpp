#include "HierarchicalStateMachine.h"

HierarchicalStateMachine::HierarchicalStateMachine(State * initialState)
{
    this->currentState = initialState;
}

void HierarchicalStateMachine::setState(State * state)
{
    this->currentState = state;
}

State * HierarchicalStateMachine::process(Event *event)
{
    State *processedState = this->currentState;

    while (processedState != nullptr)
    {
        //run event handler and save if it was consumed
        bool wasEventConsumed = processedState->onEvent(event);

        if(wasEventConsumed)
        {
            //return state that consumed the event 
            return processedState;
        }
        else
        {
            //set processed state for next loop iteration
            processedState = processedState->parent;
        }
    }

    return nullptr;
}
