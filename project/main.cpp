#include <thread>
#include "Player/Player.h"

void playerThreadFunc(void)
{
    Player player;

    player.eventLoop();
}

int main(void)
{
    std::thread playerThread(playerThreadFunc);

    playerThread.join();

    return 0;
}
