#ifndef QUEUE_H
#define QUEUE_H

/**
 * @file
 * @author  Karol Grzegorczyk
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * @section DESCRIPTION
 *
 * Inter-process queue interface
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>

///Queue handle
typedef struct Queue_t *QueueHandle_t;

/**
* Creates a Queue
*
* @param queueLength     queue item count
* @param itemSize        single queue item size
* @return                queue handle
*/
QueueHandle_t QueueCreate(const size_t queueLength, const size_t itemSize);

/**
* Enqueues one item. Blocks if queue is full.
*
* @param *queue     queue handle
* @param *msg       pointer to data which we want to send
*/
void QueueSend(QueueHandle_t queue, const void* msg);

/**
* Dequeues one item. Blocks if queue is empty.
*
* @param *queue     queue handle
* @param[out] *msg  pointer to where data will be put
*/
void QueueReceive(QueueHandle_t queue, void* msg);

/**
* Queue cleanup
*
* @param *queue     queue handle
*/
void QueueDestroy(QueueHandle_t queue);

#ifdef __cplusplus
}
#endif

#endif