#pragma once

/**
 * @file
 * @author  Karol Grzegorczyk
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * @section DESCRIPTION
 *
 * File contains concrete state definitions of player state machine
 */

#include "../HierarchicalStateMachine/State.h"

class StateOff : public State
{
public:
    /**
    * Creates state instance
    */
    StateOff(void) : State("StateOff") {}

    /**
    * StateOff event handler
    *
    * @param *event     event pointer to be processed
    * @return           true if event was processed
    */
    bool onEvent(Event * event) override;
};

class StateOn : public State
{
public:
    /**
    * Creates state instance.
    */
    StateOn(void) : State("StateOn") {}

    /**
    * StateOn event handler
    *
    * @param *event     event pointer to be processed
    * @return           true if event was processed
    */
    bool onEvent(Event * event) override;
};

class StatePlaybackStopped : public State
{
public:
    /**
    * Creates state instance, this state has StateOn parent
    */
    StatePlaybackStopped(StateOn * parent) : State(parent, "StatePlaybackStopped") {}

    /**
    * StatePlaybackStopped event handler
    *
    * @param *event     event pointer to be processed
    * @return           true if event was processed
    */
    bool onEvent(Event * event) override;
};

class StatePlaying : public State
{
public:
    /**
    * Creates state instance, this state has StateOn parent
    */
    StatePlaying(StateOn * parent) : State(parent, "StatePlaying") {}

    /**
    * StatePlaying event handler
    *
    * @param *event     event pointer to be processed
    * @return           true if event was processed
    */
    bool onEvent(Event * event) override;
};
