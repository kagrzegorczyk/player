#include "PlayerStateTransition.h"

PlayerStateTransition::PlayerStateTransition(State * startState, PlayerEvent::PlayerEvents eventType, State * endState)
{
    this->startState = startState;
    this->eventType = eventType;
    this->endState = endState;
}

bool PlayerStateTransition::doStateAndEventMatch(State * startState, PlayerEvent::PlayerEvents eventType)
{
    return (this->startState == startState && this->eventType == eventType);
}