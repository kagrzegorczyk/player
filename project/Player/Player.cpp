#include "Player.h"
#include "PlayerEvent.h"
#include <iostream>

//calls states constructors and sets initial state to StateOff
Player::Player(void) : stateOff(), stateOn(), statePlaybackStopped(&stateOn), statePlaying(&stateOn), HierarchicalStateMachine(&stateOff)
{
    this->queue = QueueCreate(QUEUE_MESSAGE_COUNT, QUEUE_MESSAGE_SIZE);
}

Player::~Player(void)
{
    QueueDestroy(queue);
}

State * Player::findMatchingTransitionState(State * stateThatConsumed, PlayerEvent::PlayerEvents eventType)
{
    for(int transitionIndex = 0; transitionIndex < sizeof(transitionTable)/sizeof(transitionTable[0]); transitionIndex++)
    {
        if((transitionTable[transitionIndex]).doStateAndEventMatch(stateThatConsumed, eventType))
        {
            return transitionTable[transitionIndex].endState;
        }
    }

    return nullptr;     //no matching state found, return null
}

void Player::eventLoop(void)
{
    while(1)
    {
        PlayerEvent event;
        char buffer[QUEUE_MESSAGE_SIZE];
        bool readResult;

        QueueReceive(this->queue, (void *) buffer); //reads data from queue to buffer, blocks here

        readResult = event.fillEventData(buffer);   //fills event data

        if(readResult)  //if managed to read valid event
        {
            State * stateThatConsumed = this->process(&event);  //processed the event

            if(stateThatConsumed != nullptr)    //if event processed
            {
                State * matchingState = findMatchingTransitionState(stateThatConsumed, event.eventType);

                if(matchingState != nullptr)
                {
                    std::cout << "Transition: " << currentState->name << " -> ";
                    setState(matchingState);    //transition takes place
                    std::cout << currentState->name << std::endl;
                }
                else
                {
                    std::cout << "No matching transition found" << std::endl;
                }
            }
            else
            {
                std::cout << "Event not consumed" << std::endl;
            }
        }
        else
        {
            std::cout << "Event incorrect" << std::endl;
        }
    }
}
