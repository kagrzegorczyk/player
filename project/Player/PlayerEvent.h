#pragma once

/**
 * @file
 * @author  Karol Grzegorczyk
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * @section DESCRIPTION
 *
 * The HierarchicalStateMachine class represents an abstract state machine.
 */

#include "../HierarchicalStateMachine/State.h"

struct PlayerEvent : public Event
{
    ///player event types
    enum PlayerEvents
    {
        EVENT_POWER_OFF,
        EVENT_POWER_ON,
        EVENT_PLAY,
        EVENT_STOP,
        EVENT_COUNT,
    };

    /**
    * Fills event data
    *
    * @param *buffer    pointer to data used to read and event
    * @return           true if data contained a valid event
    */
    bool fillEventData(char * buffer);

    enum PlayerEvents eventType;
    char data[32];
};
