#include <string.h>
#include "PlayerEvent.h"

bool PlayerEvent::fillEventData(char * buffer)
{
    //assuming that event type is stored as 1-byte unsigned value
    char readType;

    readType = buffer[0];

    if(readType < PlayerEvent::EVENT_COUNT)
    {
        eventType = (PlayerEvent::PlayerEvents) readType;
        memcpy(this->data, buffer + sizeof(readType), sizeof(this->data));

        return true;
    }

    return false;
}
