#include "PlayerStates.h"
#include "PlayerEvent.h"

bool StateOff::onEvent(Event * event)
{
    PlayerEvent *playerEvent = (PlayerEvent *) event;

    if(playerEvent->eventType == PlayerEvent::EVENT_POWER_ON)
    {
        return true;
    }

    return false;
}

bool StateOn::onEvent(Event * event)
{
    PlayerEvent *playerEvent = (PlayerEvent *) event;

    if(playerEvent->eventType == PlayerEvent::EVENT_POWER_OFF)
    {
        return true;
    }

    return false;
}

bool StatePlaybackStopped::onEvent(Event * event)
{
    PlayerEvent *playerEvent = (PlayerEvent *) event;

    if(playerEvent->eventType == PlayerEvent::EVENT_PLAY)
    {
        return true;
    }

    return false;
}

bool StatePlaying::onEvent(Event * event)
{
    PlayerEvent *playerEvent = (PlayerEvent *) event;

    if(playerEvent->eventType == PlayerEvent::EVENT_STOP)
    {
        return true;
    }

    return false;
}