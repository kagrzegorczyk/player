#pragma once

/**
 * @file
 * @author  Karol Grzegorczyk
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * @section DESCRIPTION
 *
 * The PlayerStateTransition class represents a single state transition allowed in Player state machine
 */

#include "PlayerStates.h"
#include "PlayerEvent.h"

struct PlayerStateTransition
{
    State * startState;
    PlayerEvent::PlayerEvents eventType;
    State * endState;

    /**
    * State transition constructor
    *
    * @param *startState     start state pointer
    * @param eventType       event type
    * @param *endState       end state pointer
    */
    PlayerStateTransition(State * startState, PlayerEvent::PlayerEvents eventType, State * endState);
    
    /**
    * Checks if start state and event match
    *
    * @param *startState        start state pointer
    * @param eventType          event type
    * @return         true if given parameters match to instance values
    */
    bool doStateAndEventMatch(State * startState, PlayerEvent::PlayerEvents eventType);
};



