#pragma once

/**
 * @file
 * @author  Karol Grzegorczyk
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * @section DESCRIPTION
 *
 * Player hierarchical state machine definition
 */

#include "../HierarchicalStateMachine/HierarchicalStateMachine.h"
#include "PlayerStates.h"
#include "PlayerStateTransition.h"
#include "../Queue.h"

#define QUEUE_MESSAGE_COUNT (10)
#define QUEUE_MESSAGE_SIZE (64)

class Player : public HierarchicalStateMachine
{
public:

    /**
    * Creates player state machine instance
    */
    Player(void);

    /**
    * Destructor
    */
    ~Player(void);

    ///one of machine states: StateOff
    StateOff stateOff;
    ///one of machine states: StateOn
    StateOn stateOn;
    ///one of machine states: StatePlaybackStopped
    StatePlaybackStopped statePlaybackStopped;
    ///one of machine states: StatePlaying
    StatePlaying statePlaying;

    /**
    * Enters a loop, where inter-process events are read and processed. Blocks.
    */
    void eventLoop(void);

private:
    ///array containing allowed transitions for player state machine
    PlayerStateTransition transitionTable[4] = 
    {
        PlayerStateTransition(&stateOff, PlayerEvent::EVENT_POWER_ON, &statePlaybackStopped),
        PlayerStateTransition(&stateOn, PlayerEvent::EVENT_POWER_OFF, &stateOff),
        PlayerStateTransition(&statePlaybackStopped, PlayerEvent::EVENT_PLAY, &statePlaying),
        PlayerStateTransition(&statePlaying, PlayerEvent::EVENT_STOP, &statePlaybackStopped),
    };

    /**
    * Finds a matching end state for given start state and event
    *
    * @param *stateThatConsumed     start state pointer
    * @param eventType              event type
    * @return                       matching state, nullptr, if not found
    */
    State * findMatchingTransitionState(State * stateThatConsumed, PlayerEvent::PlayerEvents eventType);

    ///interprocess queue handle
    QueueHandle_t queue;
};
