#include <mqueue.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Queue.h"

#define MSG_PRIORITY (1)

struct Queue_t
{
    mqd_t mqDescriptor;
    char * buffer;
    size_t itemSize;
    size_t bufferSize;
};

struct Queue_t ipcQueue;

QueueHandle_t QueueCreate(const size_t queueLength, const size_t itemSize)
{
    struct mq_attr attr;

    attr.mq_maxmsg = queueLength;
    attr.mq_msgsize = itemSize;
    attr.mq_flags = 0;

    ipcQueue.itemSize = itemSize;
    ipcQueue.mqDescriptor = mq_open("/player", O_RDWR | O_CREAT, 0777, &attr);
    
    if(errno != 0)
    {
        perror("mq_open()");
    }

    ipcQueue.bufferSize = itemSize * 50;
    ipcQueue.buffer = malloc(ipcQueue.bufferSize);

    if(ipcQueue.buffer == NULL)
    {
        perror("malloc()");
    }

    return &ipcQueue;
}

void QueueDestroy(QueueHandle_t queue)
{
    if(queue != NULL)
    {
        free(queue->buffer);
        mq_close(queue->mqDescriptor);
    }
}

void QueueSend(QueueHandle_t queue, const void* msg)
{
    unsigned int priority = MSG_PRIORITY; 

    if(mq_send(queue->mqDescriptor, msg, queue->itemSize, priority) == -1)
    {
        perror("mq_send()");
    }

    if(errno != 0)
    {
        perror("mq_send()");
    }
}

void QueueReceive(QueueHandle_t queue, void* msg)
{
    unsigned int priority; 

    if(mq_receive(queue->mqDescriptor, queue->buffer, queue->bufferSize, &priority) > 0)
    {
        memcpy(msg, queue->buffer, queue->itemSize);
    }

    if(errno != 0)
    {
        perror("mq_receive()");
    }
}
