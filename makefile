TARGET_EXEC ?= player
BUILD_DIR = build/$(TARGET_EXEC)
SRC_DIRS ?= project
INCLUDES = 

CC = gcc
CPP = g++

SRCS = $(shell find $(SRC_DIRS) -name *.cpp -or -name *.c -or -name *.s)
OBJS = $(SRCS:%=$(BUILD_DIR)/%.o)
DEPS = $(OBJS:.o=.d)

INC_FLAGS = $(foreach d,$(INCLUDES),-I$d)

LIBS =
CFLAGS = $(INC_FLAGS) -g -MMD -MP
ASFLAGS = $(INC_FLAGS)
CPPFLAGS = $(INC_FLAGS) -std=c++11 -g -MMD -MP
LDFLAGS = -pthread -lstdc++ -lrt

$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS)
	$(CPP) $(OBJS) $(LIBS) -o $@ $(LDFLAGS)

# assembly
$(BUILD_DIR)/%.s.o: %.s
	@$(MKDIR_P) $(dir $@)
	$(CC) $(ASFLAGS) -c $< -o $@

# c source
$(BUILD_DIR)/%.c.o: %.c
	@$(MKDIR_P) $(dir $@)
	$(CC) $(CFLAGS) -c $< -o $@

# c++ source
$(BUILD_DIR)/%.cpp.o: %.cpp
	@$(MKDIR_P) $(dir $@)
	$(CPP) $(CPPFLAGS) -c $< -o $@

-include $(DEPS)

.PHONY: clean

clean:
	$(RM) -r $(BUILD_DIR)

MKDIR_P = mkdir -p
